use sonogram::{SpecOptionsBuilder, ColourGradient, FrequencyScale};

use std::fs::File;
use std::io::{self, BufRead, BufReader};
use pyo3::prelude::*;
use serde::{Serialize, Deserialize}; // for JSON serialization
use somelib; // hypothetical SOM library

// Define the SOM structure
#[derive(Serialize, Deserialize)]
struct SOM {
    // fields representing the state of your SOM
}

impl SOM {
    fn new() -> SOM {
        // initialize a new SOM
    }

    fn train(&mut self, data: Vec<Vec<f64>>) {
        // train the SOM with the provided data
    }

    fn predict(&self, input: Vec<f64>) -> Vec<f64> {
        // make a prediction based on the input
    }

    fn from_json(json_str: &str) -> PyResult<Self> {
        // load SOM from JSON string
        serde_json::from_str(json_str).map_err(|e| PyErr::new::<pyo3::exceptions::PyValueError, _>(format!("Failed to parse JSON: {}", e)))
    }

    fn to_json(&self) -> PyResult<String> {
        // serialize SOM to JSON string
        serde_json::to_string(self).map_err(|e| PyErr::new::<pyo3::exceptions::PyValueError, _>(format!("Failed to serialize to JSON: {}", e)))
    }
}

#[pyfunction]
fn create_som() -> PyResult<SOM> {
    Ok(SOM::new())
}

#[pyfunction]
fn train_som(som: &mut SOM, data: Vec<Vec<f64>>) {
    som.train(data);
}

#[pyfunction]
fn predict_som(som: &SOM, input: Vec<f64>) -> PyResult<Vec<f64>> {
    Ok(som.predict(input))
}

#[pyfunction]
fn som_from_json(json_str: String) -> PyResult<SOM> {
    SOM::from_json(&json_str)
}

#[pyfunction]
fn som_to_json(som: &SOM) -> PyResult<String> {
    som.to_json()
}

// PyO3 Module
#[pymodule]
fn som_module(py: Python, m: &PyModule) -> PyResult<()> {
    
    Ok(())
}

fn read_numbers_from_file(file_path: &str) -> Result<Vec<f64>, io::Error> {
    let file = File::open(file_path)?;
    let reader = BufReader::new(file);
    let mut numbers = Vec::new();

    for line in reader.lines() {
        let line = line?;
        match line.trim().parse::<f64>() {
            Ok(number) => numbers.push(number),
            Err(e) => eprintln!("Failed to parse number: {} (error: {})", line, e),
        }
    }

    Ok(numbers)
}
  

pub static VERSION: &str = env!("CARGO_PKG_VERSION");

// #[pyfn(m)]
#[pyfunction]
#[pyo3(text_signature = "(signal_file_path, output_file_path, sample_rate, /)")]
fn generate_spectrogram(
    py: Python,
    signal_file_path: String,
    output_file_path: String,
    sample_rate: u32,
) -> PyResult<Py<PyAny>> {
        let png_file = std::path::Path::new(&output_file_path);
    match read_numbers_from_file(&signal_file_path) {
        Ok(numbers) => {
            let waveform: Vec<i16> = numbers.iter()
                                            .map(|&n| (n*10000.0).round() as i16)
                                            .collect();

            // Build the model
            let mut spectrograph = SpecOptionsBuilder::new(2048)
            .load_data_from_memory(waveform, sample_rate)
            .build().unwrap().compute();


            // Specify a colour gradient to use (note you can create custom ones)
            let mut gradient = ColourGradient::rainbow_theme();   // Green
            // Save the spectrogram to PNG.
            

            spectrograph.to_png(&png_file, 
                FrequencyScale::Linear,
                &mut gradient,
                1024,    // Width
                1024,    // Height
            ).unwrap();
        }
        Err(e) => println!("Failed to read file: {}", e),
    }
    Ok(png_file.into_py(py))
}

 #[pymodule]
 fn wheelbearing_detection(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(generate_spectrogram, m)?)?;
    m.add_function(wrap_pyfunction!(create_som, m)?)?;
    m.add_function(wrap_pyfunction!(train_som, m)?)?;
    m.add_function(wrap_pyfunction!(predict_som, m)?)?;
    m.add_function(wrap_pyfunction!(som_from_json, m)?)?;
    m.add_function(wrap_pyfunction!(som_to_json, m)?)?;
    Ok(())
 }
